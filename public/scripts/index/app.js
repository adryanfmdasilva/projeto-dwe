let clicked = false;
let menu = document.querySelector(".nav-list-mobile");
let menuLI = document.querySelectorAll(".nav-list-mobile-item");
document.querySelector(".mobile-menu").addEventListener('click', function() {
    clicked = !clicked;

    if(clicked){
        menu.classList.add("active");
        menuLI.forEach(element => {
            element.style.opacity=1
        });
    } else {
        menu.classList.remove("active");
        menuLI.forEach(element => {
            element.style.opacity=0
        });
    }
});

function menuLiClick(){
    clicked = false;
    menu.classList.remove("active");
        menuLI.forEach(element => {
            element.style.opacity=0
        });
}

menuLI.forEach(element => {
    element.addEventListener('click', menuLiClick);
});